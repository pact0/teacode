import {
  Avatar,
  Checkbox,
  FormControlLabel,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@material-ui/core";
import React from "react";
import { useDispatch } from "react-redux";
import { toggleChecked } from "../reducers/userReducer";

const SingleUser = ({ user }) => {
  const dispatch = useDispatch();

  const handleToggle = () => {
    dispatch(toggleChecked(user?.id));
  };

  return (
    <ListItem
      key={user?.id}
      role={undefined}
      dense
      button
      onClick={handleToggle}
    >
      <ListItemAvatar>
        <Avatar alt={`Avatar n°${user?.id}`} src={user?.avatar} />
      </ListItemAvatar>
      <ListItemText
        id={user?.id}
        primary={user?.first_name + " " + user?.last_name}
      />

      <FormControlLabel control={<Checkbox checked={user?.checkbox} />} />
    </ListItem>
  );
};

export default SingleUser;
