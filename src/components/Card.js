import { Input, InputAdornment } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import UsersList from "./UsersList";
import SearchIcon from "@material-ui/icons/Search";

const Card = () => {
  const allUsers = useSelector((state) => state.users);
  const [value, setValue] = useState("");
  const [users, setUsers] = useState([]);

  useEffect(() => {
    setUsers(allUsers);
    console.log(
      "Checkbox-on: ",
      allUsers.filter((user) => user.checkbox === true)
    );
  }, [allUsers]);

  return (
    <>
      <div>
        <Input
          id="input-with-icon-adornment"
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          }
          value={value}
          onChange={(e) => setValue(e.target.value)}
          placeholder="first name  last name..."
          style={{ margin: "10px 15px" }}
        />
      </div>

      <UsersList users={users} filter={value} />
    </>
  );
};

export default Card;
