import React, { useEffect, useState } from "react";
import { List } from "react-virtualized";
import SingleUser from "./SingleUser";

function UsersList({ users, filter }) {
  const [filteredUsers, setFilteredUsers] = useState(users);

  useEffect(() => {
    if (!!users) {
      const look = filter.toLowerCase();
      const filteredUsers = users.filter((user) => {
        if (
          user.first_name.toLowerCase().search(look) !== -1 ||
          user.last_name.toLowerCase().search(look) !== -1
        )
          return user;
        return undefined;
      });
      setFilteredUsers(filteredUsers);
    }
  }, [filter, users]);

  const renderRow = ({ index, key, style }) => (
    <div key={key} style={style} className="post">
      <SingleUser user={filteredUsers[index]} />
    </div>
  );

  return (
    <div>
      <List
        width={720}
        height={920}
        rowRenderer={renderRow}
        rowCount={filteredUsers.length}
        rowHeight={60}
        style={{ "scrollbar-width": "none", margin: "10px 0px" }}
      />
    </div>
  );
}
export default UsersList;
