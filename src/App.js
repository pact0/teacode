import { useEffect } from "react";
import { useDispatch } from "react-redux";
import "./App.css";
import Card from "./components/Card";
import { initializeUsers } from "./reducers/userReducer";

const url =
  "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    function getData() {
      fetch(url)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          dispatch(initializeUsers(data));
        });
    }
    getData();
  }, [dispatch]);

  return (
    <div
      style={{
        display: "flex",
        "flex-direction": "column",
        "align-items": "center",
      }}
    >
      <h2>Contacts:</h2>
      <Card />
    </div>
  );
}

export default App;
