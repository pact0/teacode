const initialState = [];

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_ALL_USERS":
      return action.data
        .sort((a, b) => {
          if (a.last_name < b.last_name) {
            return -1;
          }
          if (a.last_name > b.last_name) {
            return 1;
          }
          return 0;
        })
        .map((user) => {
          return { ...user, checkbox: false };
        });
    case "TOGGLE_CHECKED": {
      const newState = state.map((user) => {
        if (user.id === action.data) {
          return { ...user, checkbox: !user.checkbox };
        }
        return user;
      });

      return newState;
    }

    default:
      return state;
  }
};

export const initializeUsers = (data) => {
  return {
    type: "SET_ALL_USERS",
    data: data,
  };
};
export const toggleChecked = (id) => {
  return {
    type: "TOGGLE_CHECKED",
    data: id,
  };
};

export default userReducer;
