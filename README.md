# teacode

To run this project locally type into your console

`git clone https://gitlab.com/pact0/teacode.git`

to download the repository, then

`cd teacode`

to go into directory

`yarn install`

to install required packages, and finally

`yarn start`

to run the app locally.
The app should be running now!


My OS of choice is linux.
